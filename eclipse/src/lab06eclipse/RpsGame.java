package lab06eclipse;
import java.util.*;

public class RpsGame {
	private int numOfwins = 0;
	private int ties = 0;
	private int losses = 0;
	private Random rand = new Random();
	
	public int getWins() {
		return this.numOfwins;
	}
	
	public int getTies() {
		return this.ties;
	}
	
	public int getLosses() {
		return this.losses;
	}
	
	public String playRound(String move) {
		int RandomMove = rand.nextInt(3);
		String ComputerMove;
		
		if(RandomMove == 1) {
			ComputerMove = "Rock";
		}else if(RandomMove == 2) {
			ComputerMove = "Paper";
		}else{
			ComputerMove = "Scissors";
		}
		
		boolean winner = true;
		
		if(!(move.toUpperCase().equals("ROCK") || move.toUpperCase().equals("PAPER") || move.toUpperCase().equals("SCISSORS"))) {
			return "Invalid User Input";
		}else if(move.toUpperCase().equals(ComputerMove.toUpperCase())) {
			this.ties++;
			return "A Tie"; 
		}else if(move.toUpperCase().equals("ROCK") && ComputerMove.toUpperCase().equals("SCISSORS")) {
			this.numOfwins++;
		}else if (move.toUpperCase().equals("SCISSORS") && ComputerMove.toUpperCase().equals("PAPER")) {
			this.numOfwins++;
		}else if (move.toUpperCase().equals("PAPER") && ComputerMove.toUpperCase().equals("ROCK")) {
			this.numOfwins++;
		}else {
			this.losses++;
			winner = false;
		}
		
		if(winner) {
			return "Computer Played " + ComputerMove + " And The Computer Lost"; 
		}else {
			return "Computer Played " + ComputerMove + " And The Computer Won"; 
		}
	}
	
}
