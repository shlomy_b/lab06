//Shlomo Bensimhon

package lab06eclipse;
import java.util.*;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsApplication extends Application {	
	private RpsGame r1 = new RpsGame();

	public void start(Stage stage) {
		Group root = new Group(); 
		
		VBox overall = new VBox();
		HBox buttons = new HBox();
		Button b1 = new Button("Rock");
		Button b2 = new Button("Paper");
		Button b3 = new Button("Scissors");
		
		HBox textFields = new HBox();
		TextField tf1 = new TextField("Welcome!");
		TextField tf2 = new TextField("Wins");
		TextField tf3 = new TextField("Losses");
		TextField tf4 = new TextField("Ties");
		tf1.setPrefWidth(300);

		RpsChoice actionRock = new RpsChoice(tf1, tf2, tf3, tf4, "Rock", r1);
		b1.setOnAction(actionRock);
		
		RpsChoice actionPaper = new RpsChoice(tf1, tf2, tf3, tf4, "Paper", r1);
		b2.setOnAction(actionPaper);
		
		RpsChoice actionScissors = new RpsChoice(tf1, tf2, tf3, tf4, "Scissors", r1);
		b3.setOnAction(actionScissors);
		
		buttons.getChildren().addAll(b1, b2, b3);
		textFields.getChildren().addAll(tf1, tf2, tf3, tf4);
		overall.getChildren().addAll(buttons, textFields);
		root.getChildren().add(overall);

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.CYAN);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    


