//Shlomo Bensimhon

package lab06eclipse;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent> {
	private TextField tf1;
	private TextField tf2;
	private TextField tf3;
	private TextField tf4;
	private String playerMove;
	private RpsGame r1;

	public RpsChoice(TextField tf1, TextField tf2, TextField tf3,TextField tf4, String playerMove, RpsGame r1) {
		this.tf1 = tf1;
		this.tf2 = tf2;
		this.tf3 = tf3;
		this.tf4 = tf4;
		this.playerMove = playerMove;
		this.r1 = r1;
	}
	

	public void handle(ActionEvent e) {
		String result = r1.playRound(this.playerMove);
		this.tf1.setText(result);
		this.tf2.setText("Wins: " + r1.getWins());
		this.tf3.setText("Losses: " + r1.getLosses());
		this.tf4.setText("Ties: " + r1.getTies());
	}
}










